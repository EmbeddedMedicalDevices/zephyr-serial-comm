# Zephry: Serial Communication

## Serial vs. Parallel

### What is **Serial** Communication?

Send data one bit at a time, sequentially, over a single wire; in contrast to parallel communication, where multiple bits are sent as a whole, on multiple wires.

Common examples:

* RS-232
* USB
* SATA/SCSI
* PS/2
* Ethernet
* HDMI/DVI
* PCIe (not PCI!)

![serial_parallel_comm](Serial_and_Parallel_Data_Transmission.svg.png)

### Why Serial over Parallel?

* Parallel communication can communicate more bits / clock cycle, but serial links can be clocked much faster.
* No clock skew between bits
* Fewer cables / connections (cheaper)
* Better isolation from noise / interference / crosstalk

## Universal Asynchronous Receiver/Transmitter (UART)

UART is an **asynchronous** serial communication protocol. It is a common peripheral on microcontrollers, and is used to communicate with other devices, such as sensors, over a serial link.

![uart_timing](UART_Timing_Diagram.png)

* `Start bit` is usually `LOW`, followed by 8 bits of data.
* A `parity` bit--conveying an even of odd data stream--can be used for error checking.

### Tx/Rx without Flow Control

![uart_tx_rx](UART_Tx_Rx.png)

* `RX` senses the start bit and then stores the subsequent bits in a shift register (making a "word").
* The data *baud* rate between `TX:RX` needs to be established between the two devices.  This is typically 9600 or 115200 bits/s.

### Tx/Rx with Flow Control

![uart_flow_control](UART_Tx_Rx_FlowControl.png)

`RTS` (Request to Send) and `CTS` (Clear to Send) are used to control the flow of data between the two devices.

### UART Firmware API Events

![uart_api](UART_API.png)

* UART ISRs are triggered by `RTS` and `CTS` events.
* High-priority event helps avoid missing data, but can cause starvation of other tasks.
* Low-priority event can cause data loss, but allows other tasks to run.
* Multi-threaded RTOS can be much more robust than a single-threaded super-loop system.

## Inter-Integrated Circuit (I2C)

![i2c](I2C.png)

* Widely-used 2-wire serial communication protocol.
* Limited to short distances.
* `SCL`: serial clock
* `SDA`: serial data
* Common data communication rates: 100, 400, & 1000 kbps
* Access data using simple `read()` and `write()` functions.
* Multiple devices can be connected to the same bus, but each device must have a unique address (7- or 10-bits).

### Alternative Master/Slave Terminology

![i2c_terminology](I2C_AltTerm.png)
* Primary/Secondary
* Controller/Peripheral
* Controller/Responder
* Parent/Child

### Nordic TWI (Two-Wire Interface)

* Nordic's I2C implementation is called TWI (Two-Wire Interface).

## UART vs. I2C

* UART is a point-to-point protocol, while I2C is a multi-point protocol.
* UART is asynchronous (2-way communication, full duplex), while I2C is synchronous (half duplex, needs `CLK`).
* Both are "slow", but I2C is faster.
* UART is simpler to implement, but I2C is more robust.

## SenseWire (I3C)

* 2-pin superset of I2C (backward compatible)
* Lower power and space requirements
* Higher data rates
* Dynamic address assignment
* "Hot" peripheral connection

## Serial Passing Interface (SPI)

![spi](SPI.png)

* Like I2C, but allow for full duplex (concurrent send/receive of data).
* Needs 4 wires:
  * `MOSI`: master out, slave in
  * `MISO`: master in, slave out
  * `SCLK`: serial clock
  * `SS`: slave select (`LOW` to select)
* `SS` instead of device address
* Higher data speeds than I2C (e.g., SD card)
* More complicated connectivity scheme for multiple peripherals


## Zephyr: Sensors

* The reading of data from sensors is so common that Zephyr provides a common API for accessing them.
  * **Channels:** measurable quantities (e.g., temperature, humidity, acceleration, etc.)
  * **Fetching** data from a sensor is done using [`sensor_sample_fetch()`](https://docs.zephyrproject.org/latest/hardware/peripherals/sensor.html#c.sensor_sample_fetch) function.  Fetching stores the data in a buffer on the sensor.
  * **Getting** data from the sensor is then done using [`sensor_sample_get()`](https://docs.zephyrproject.org/latest/hardware/peripherals/sensor.html#c.sensor_channel_get).  Getting reads the data from the buffer and returns it to the user.
    * A sensor can have multiple channels, and each channel can have multiple samples.
    * Sensor channels are specified using a `sensor_channel` enum: [`sensor_channel`](https://docs.zephyrproject.org/latest/doxygen/html/group__sensor__interface.html#gaaa1b502bc029b10d7b23b0a25ef4e934)
  * [`sensor_value`](https://docs.zephyrproject.org/latest/hardware/peripherals/sensor.html#c.sensor_value) struct of 2 ints
    1. `int32_t val1`: integer part of value
    1. `int32_t val2`: fractional part of value
  * Helper functions exist to convert values (units, struct -> float, etc.)
    * [`sensor_value_to_float()`](https://docs.zephyrproject.org/latest/hardware/peripherals/sensor.html#c.sensor_value_to_float)

## Example Firmware

See [application/](application/) for an example library.

## Example Connection of MCP9808 Temperature Sensor

https://www.adafruit.com/product/1782

![mcp9808](mcp9808.png)

|Breakout Board|DK|
|--------------|--|
| `SDA` | `P0.26` |
| `SCL` | `P0.27` |
| `Vdd` | `VDD` |
| `GND` | `GND` |


## Resources
* https://en.wikipedia.org/wiki/Serial_communication
* [DevAcademy: UART](https://academy.nordicsemi.com/lessons/lesson-4-serial-communication-uart/)
* [DevAcademy: I2C](https://academy.nordicsemi.com/lessons/lesson-6-serial-com-i2c/)
* https://www.allaboutcircuits.com/news/how-master-slave-terminology-reexamined-in-electrical-engineering/
* https://docs.zephyrproject.org/latest/hardware/peripherals/sensor.html
* https://github.com/zephyrproject-rtos/zephyr/tree/main/samples/sensor/mpr
